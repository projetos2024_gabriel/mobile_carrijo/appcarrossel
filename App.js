import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import CountImage from "./src/countImage";
import HomeScreen from "./layout/home"

const Stack = createStackNavigator();


export default function App() {
  return (
      <NavigationContainer>
    <Stack.Navigator initialRouteName='Home'>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="CountMoviment" component={CountMoviment} />
        <Stack.Screen name="CountImage" component={CountImage} />
      </Stack.Navigator>
   </NavigationContainer>
  );
}

