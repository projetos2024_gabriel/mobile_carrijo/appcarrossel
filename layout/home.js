import React from "react";
import { View, Button, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
  const navigation = useNavigation();
  
  const handleImage = () => {
    navigation.navigate("CountImage");
  };
  
  const handleCount = () => {
    navigation.navigate("CountMoviment");
  };

  return (
    <View style={styles.container}>
      <View style={styles.square}>
        <Button title="Ir para As Imagens" onPress={handleImage} />
      </View>
      <View style={styles.square}>
        <Button title="Ir para O Contador" onPress={handleCount} color="blue" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  square: {
    width: 200,
    height: 250,
    margin: 25,
    backgroundColor: "#eee",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
});
    
export default HomeScreen;
