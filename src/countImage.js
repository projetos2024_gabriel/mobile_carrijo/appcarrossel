import React, { useRef, useState } from "react";
import { View, Text, Dimensions, PanResponder, Image, StyleSheet } from "react-native";

const images = [
    'https://i.pinimg.com/564x/45/a4/0a/45a40a6a181e8dee3a6656ac62013142.jpg',
    'https://i.pinimg.com/564x/5d/78/0a/5d780ad289457d1542324af2bf83a59c.jpg',
    'https://i.pinimg.com/564x/5c/ac/6c/5cac6c70d076e500a2066da8f177921f.jpg'
];

export default function ImageSlider() {
    const [cont, setCount] = useState(0);
    const screenWidth = Dimensions.get("window").width;
    const gestureThreshold = screenWidth * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                
            },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx > gestureThreshold) {
                    // Movimento para a direita, retrocede na imagem
                    setCount((prevCount) => (prevCount > 0 ? prevCount - 1 : prevCount));
                } else if (gestureState.dx < -gestureThreshold) {
                    // Movimento para a esquerda, avança na imagem
                    setCount((prevCount) => (prevCount < images.length - 1 ? prevCount + 1 : prevCount));
                }
            },
        })
    ).current;

    return (
        <View
            {...panResponder.panHandlers}
            style={styles.container}
        >
            <Image
                source={{ uri: images[cont] }}
                style={styles.image}
            />
            <Text style={styles.text}>Imagem {cont + 1}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#f0f0f0",
    },
    image: {
        width: 290,
        height: 245,
        resizeMode: 'cover',
        marginBottom: 20,
    },
    text: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#333",
    },
});
